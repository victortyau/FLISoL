# FLISoL 2019
14º Festival Latinoamericano de Instalación de Software Libre el 27 de abril 2019. En Panamá se estara celebrando en diferente localidades. https://flisol.info/
#### Fecha:  27 de Abril del 2019
#### Lugar: Auditorio de la Universidad Interamericana de Panamá (UIP)
#### Horario: 9:00 am a 3:00 pm

|       EXPOSITORES  |                           TEMA                 | HORARIO  |
|--------------------|------------------------------------------------|----------|
|FLOSSPA Team        |Bienvenida                                      |09:00 - 09:05  |
|       |                    |09:05 - 09:30  |
|   |                                  |09:30 - 10:00  |
|        |                          |10:00 - 10:30  |
|        |                             |10:30 - 11:00  |
|      | |11:00 - 11:30  |
| Nelly Valdivieso | Colaboración DiseñadorUX/UI con Programadores      |11:30 - 12:00  |
|Almuerzo            |Almuerzo                                        |12:00 - 12:30  |
|                   |12:30 - 01:00  |
| | Victor Tejada Yau   |   Beneficios de la colaboración en el Open Source                 |01:00 - 01:30  |
|   |          |01:30 - 02:00  |
|    |                |02:00 - 02:30  |
|       |  |02:30 - 03:00  |

## Organizado por:
- ### [Comunidad de Floss Panamá](https://floss-pa.net/) y Falculta de Ingenieria UIP
- ###### [Andres Abadias](https://twitter.com/David25LO), [Shelsy Chanis](https://twitter.com/shelsxacm), [Josue](), [Valeria Vivero](), [Juan]() y [Maryon Torres](https://twitter.com/maryitotr)

## Patrocinadores
[<img src="https://www.uip.edu.pa/wp-content/uploads/2017/08/Logo-UIP-jpg.jpg" width="170">](https://www.uip.edu.pa)
[<img src="https://pbs.twimg.com/profile_images/852597051808522240/5iJqsWQL_400x400.jpg" width="170">](https://www.uip.edu.pa)
[<img src="https://getfedora.org/static/images/fedora-logotext.png" width="170">](https://getfedora.org )

